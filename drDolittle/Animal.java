package drDolittle;

public abstract class Animal extends Object {
	String name;
	int lifespan;
	
	abstract void print();
}
