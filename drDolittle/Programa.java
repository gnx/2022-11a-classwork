package drDolittle;

public class Programa extends Object {
	public static void main(String[] args) {
		Elephant john = new Elephant();
		john.name = "Джон";
		
		Walking[] arr = {new Elephant(), new Giraffe()};
		
		Giraffe dave = new Giraffe();
		dave.name = "Дейв";
		dave.lifespan = 28;
		
		arr[0].walk();
//		dave.print();
	}
}
