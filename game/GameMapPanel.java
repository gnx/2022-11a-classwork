package game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameMapPanel extends Panel {
	Map map;
	
	GameMapPanel(Map m) { 
		map = m;
	}
	
	public void paint(Graphics g) {
		BufferedImage img = null;
		File f = new File("hero.png");
		try {
			img = ImageIO.read(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		g.drawImage(img, map.hero.posx * 30, map.hero.posy * 30, null);
		for (int y = 0; y < map.sizey; y++) {
			for (int x = 0; x < map.sizex; x++) {
				g.drawRect(x * 30, y * 30, 30, 30);
			}
		}
	}
}
