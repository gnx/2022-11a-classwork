package game;

import java.io.Serializable;
import java.util.Scanner;

public class Map implements Serializable {
	int sizex;
	int sizey;
	Hero hero;
	transient Scanner scan;
	
	Map(int x, int y) {
		sizex = x;
		sizey = y;
	}

	void print() {
		for (int i = 0; i < sizey; i++) {
			for (int j = 0; j < sizex; j++) {
				if (hero.posx == j && hero.posy == i) {
					System.out.print(1 + " ");
				} else {
					System.out.print('.' + " ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}
}
