package game;

public class Utility {
	public static Hero fight(Hero h1, Hero h2) {
		while(h1.health != 0 && h2.health != 0) {
			if (h2.health > 0) {
				h1.health--;
			} else if (h1.health > 0) {
				h2.health--;
			}
		}
		
		if (h1.health > 0) {
			return h1;
		} else {
			return h2;
		}
	}
}
