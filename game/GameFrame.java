package game;

import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;

public class GameFrame extends Frame {
	GameMapPanel mapPanel;
	Map map;
	Label label;
	TextField textField;
	TextArea textArea;
	Button button;
	
	GameFrame(Map m) {
		map = m;
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);
		
		label = new Label();
		label.setLocation(250, 50);
		label.setSize(100, 30);
		this.add(label);
		
		textField = new TextField();
		textField.setLocation(250, 110);
		textField.setSize(100, 30);
		this.add(textField);
		
		textArea = new TextArea();
		textArea.setLocation(250, 170);
		textArea.setSize(200, 150);
		textArea.setEnabled(false);
		this.add(textArea);
		
		button = new Button();
		button.setLabel("Start");
		button.setLocation(50, 250);
		button.setSize(150, 30);
		GameActionListener gal = new GameActionListener(this);
		button.addActionListener(gal);
		this.add(button);
		
		mapPanel = new GameMapPanel(map);
		mapPanel.setLocation(50, 50);
		mapPanel.setSize(map.sizex * 30 + 1, map.sizey * 30 + 1);
		GameKeyListener gkl = new GameKeyListener(this);
		mapPanel.addKeyListener(gkl);
		GameMouseListener gml = new GameMouseListener(this);
		mapPanel.addMouseListener(gml);
		this.add(mapPanel);
		
		this.setLayout(null);
	}
}
