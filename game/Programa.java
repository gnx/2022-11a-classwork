package game;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class Programa {
	public static void main(String[] args) {
//		Map m;
//		// десериализация
//		try {
//			FileInputStream fis = new FileInputStream("map.bin");
//			ObjectInputStream ois = new ObjectInputStream(fis);
//			m = (Map) ois.readObject();
//		} catch (IOException | ClassNotFoundException e) {
//			m = new Map(6, 5);
//			Hero h = new Hero(0, 0, m);
//			m.hero = h;
//		}
//		
//		GameFrame f = new GameFrame(m);
//		f.setSize(500, 500);
//		f.setVisible(true);
//		
//		Scanner scan = new Scanner(System.in);
//		while (true) {
//			char c = scan.next().charAt(0);
//			if (c == 'e') {
//				// сериализация
//				try {
//					FileOutputStream fos = new FileOutputStream("map.bin");
//					ObjectOutputStream oos = new ObjectOutputStream(fos);
//					oos.writeObject(m);
//					oos.close();
//					fos.close();
//				} catch (FileNotFoundException e) {
//					e.printStackTrace();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//				System.exit(0);
//			}
//			m.hero.move(c);
//			m.print();
//			f.mapPanel.repaint();
//		}
		
		Map m = new Map(5, 5);
		Hero h1 = new Hero(3, 4, m);
		h1.health = 50;
		Hero h2 = new Hero(3, 4, m);
		h2.health = 60;
		System.out.println(Utility.fight(h1, h2));
	}
}
